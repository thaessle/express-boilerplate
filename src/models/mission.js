module.exports = (sequelize, DataTypes) => {
  const Mission = sequelize.define('Mission', {
      id:{
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      objective: DataTypes.TEXT,
      reward: DataTypes.DECIMAL,
      type: {
        type: DataTypes.ENUM,
        values: ['development', 'design', 'marketing', 'testing'],
      },
      status: {
        type: DataTypes.ENUM,
        values: ['draft', 'active', 'booked', 'close'],
      },
      imageUrl: {
        type: DataTypes.STRING,
        comment: 'Url to the picture file',
        validate: {
          isUrl: true,
        },
      },
    }
  );
  return Mission;
};
